(ns foo.core
  (:require [foo.input :as input]
            [play-clj.core :refer :all]
            [play-clj.g2d  :refer :all]))

(defscreen main-screen
  :on-key-down input/handle-input

  :on-show
  (fn [screen entities]
    ;;(update! screen :renderer (stage))
    (update! screen
             :camera (orthographic)
             :renderer (orthogonal-tiled-map "path.tmx" 1))
    (assoc (texture "clj-logo.png")
      :x 5 :y 5
      :width 10 :height 10))

  :on-render
  (fn [screen entities]
    (position! screen 325 325)
    (size! screen 650 650)
    (clear!)
    (render! screen entities))

  :on-resize
  (fn [screen entities]
    (height! screen 50)))

(defgame foo-game
  :on-create #(set-screen! % main-screen))
